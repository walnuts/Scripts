#!/bin/bash
# Usage: ./cfn_flip_dir.sh <some_directory>
# Output: Flipping <file.json> to YAML

if [ -z ${1+x} ]; then
    echo "Usage: ./cfn_flip_dir.sh <some_directory>"
    exit 1
fi

if ! which cfn-flip &> /dev/null; then
    echo "Please install cfn-flip to continue."
    exit 1
fi

FLIP_DIR=$1

if ! find -f "${FLIP_DIR}"/*.json &> /dev/null; then
    echo "${FLIP_DIR} does not contain json files."
    exit 1
fi

for FILE in "${FLIP_DIR}"/*.json; do
    echo "Flipping ${FILE} to YAML"
    cfn-flip "${FILE}" "${FLIP_DIR}"/"${FILE%?????}".yaml
done
