#!/bin/bash
# Usage: ./domain_expiry.sh <example.com>
# Output: <example.com> expires on: YYYY-MM-DD at HH:MM:SS

if [ -z ${1+x} ]
then
   echo "Usage: ./domain_expiry.sh <example.com>"
   exit 1
fi

DOMAIN=$1

EXPIRATION_LINE=$(whois "$DOMAIN" | \
   grep -v "Registrar Registration Expiration Date:" | \
   grep -E '(Expiration Date|Expiry)')
EXPIRATION_DATE=$(echo "${EXPIRATION_LINE#*:}" | cut -d "T" -f 1 | xargs echo)
EXPIRATION_TIME=$(echo "${EXPIRATION_LINE#*:}" | cut -d "T" -f 2 | xargs echo)

echo "${DOMAIN} expires on: ${EXPIRATION_DATE} at ${EXPIRATION_TIME%Z*}"