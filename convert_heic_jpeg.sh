#!/bin/bash
# Usage: convert_heic_jpeg.sh <HEIC IN FILENAME>
# Output: Converting <INFILE> to <OUTFILE>

# Check if we have required executable.
if ! which magick >> /dev/null; then
    echo "This script requires imagemagick. Please install."
    exit 1
fi

FILE_NAME=$1
NO_EXTENSION="${FILE_NAME%?????}"
EXTENSION=$(echo "${FILE_NAME#"${NO_EXTENSION}"}"| tr '[:lower:]' '[:upper:]')
OUT_FILE_NAME="${NO_EXTENSION}.jpeg"

# Check if FILENAME ends in HEIC
if ! [ "${EXTENSION}" == ".HEIC" ]; then
    echo "The provided file is not HEIC. Exiting."
    exit 1
fi

echo "Converting ${FILE_NAME} to ${OUT_FILE_NAME} at 90% quality..."
magick "${FILE_NAME}" -quality 90% "${OUT_FILE_NAME}" >> /dev/null
echo "Done."